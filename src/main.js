import Vue from 'vue';
import App from './App.vue';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import Routes  from './routes.js';

const router = new VueRouter({

  routes:Routes,
  mode: 'history'
});

Vue.use(VueResource);
Vue.use(VueRouter);

new Vue({
  el: '#app',
  render: h => h(App),
  router:router
})
