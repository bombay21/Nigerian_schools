
import Contact from './components/Contact.vue'
import About from './components/About.vue'
import Home from './components/Home.vue'
import Schooldetail from './components/Schooldetail.vue'
import Edit from './components/Edit.vue'


export default [

    {path:'/', component:Home},
    {path:'/about', component:About},
    
    {path:'/contact', component:Contact},
    {path:'/school/:id', component:Schooldetail},
    {path:'/edit/:id', component:Edit}

];